﻿using Moq;
using DomainServices.Repositories;
using Xunit;
using ApplicationServices;
using FluentAssertions;
using System;

namespace UnitTests.BO
{
    public class CustomerBO_UnitTests
    {
        Mock<ICustomerRepository> repository;

        public CustomerBO_UnitTests()
        {
            repository = new Mock<ICustomerRepository>();
        }

        [Fact]
        public void GetCustomerDeliveryAddress_When_CustomerNotFound_Then_ShouldReturnNull()
        {
            // Arrange
            repository.Setup(c => c.Get(It.IsAny<int>()))
                .Returns<string>(null);

            var target = new CustomerBO(repository.Object);

            // Act
            var result = target.GetCustomerDeliveryAddress(default(int));

            // Assert
            result.Should().BeNull();
        }

        [Fact]
        public void GetCustomerDeliveryAddress_When_CustomerMoreThenOne_Then_ShoudThrowExeption()
        {
            // Arrange
            repository.Setup(c => c.Get(It.IsAny<int>()))
                .Throws<InvalidOperationException>();

            var target = new CustomerBO(repository.Object);

            // Act
            Action result = () => target.GetCustomerDeliveryAddress(default(int));
            
            // Assert
            result.Should().Throw<InvalidOperationException>();
        }
        }
    }

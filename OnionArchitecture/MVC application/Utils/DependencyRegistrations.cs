﻿using ApplicationServices;
using DAL;
using DomainServices.Repositories;
using Repositories;
using Shared.HTTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_application.Utils
{
    public class DependencyRegistrations : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            var scheme = System.Configuration.ConfigurationManager.AppSettings["ApiScheme"];
            var host = System.Configuration.ConfigurationManager.AppSettings["ApiHost"];

            Bind<IHTTPManager>()
                .To<HTTPManager>()
                .WithConstructorArgument("scheme", scheme)
                .WithConstructorArgument("host", host);

            
        }
    }
}
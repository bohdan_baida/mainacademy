﻿using ApplicationServices.Identity;
using DAL;
using DomainCore.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MVC_application.App_Start
{
    public class IdentityConfig
    {
        public void Configure(IAppBuilder app)
        {
            var connString = ConfigurationManager.ConnectionStrings["AuthConnectionString"].ConnectionString;

            app.CreatePerOwinContext(() => new IdentityContext(connString));
            app.CreatePerOwinContext<UserBO>(UserBO.Create);
            app.CreatePerOwinContext<SigninBO>(SigninBO.Create);

         

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<UserBO, User>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

        }
    }
}
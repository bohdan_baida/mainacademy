﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using MVC_application.App_Start;
using Owin;

[assembly: OwinStartup(typeof(MVC_application.Startup))]

namespace MVC_application
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            (new IdentityConfig()).Configure(app);
        }
    }
}

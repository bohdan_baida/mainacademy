﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MVC_application.Hubs
{
    public class ChatHub : Hub
    {
        static List<ChatUser> Users = new List<ChatUser>();

        // Отправка сообщений
        public void Send(string name, string message)
        {
            Clients.Caller.addMessage(name, message);
            Clients.Caller.addMessage("Support", "I think it`s impossible:(");
        }

        public void Connect()
        {
            var id = Context.ConnectionId;

            if (Users.Count(x => x.ConnectionId == id) == 0)
            {
                Users.Add(new ChatUser { ConnectionId = id});

                Clients.Caller.addMessage("Support", "How can I help you?");
                this.OnConnected();
            }
        }


        public override Task OnDisconnected(bool stopCalled)
        {
            var item = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                Users.Remove(item);
                var id = Context.ConnectionId;
            }

            return base.OnDisconnected(stopCalled);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_application.Hubs
{
    public class ChatUser
    {
        public string ConnectionId { get; set; }
        public string Name { get; set; }
    }
}
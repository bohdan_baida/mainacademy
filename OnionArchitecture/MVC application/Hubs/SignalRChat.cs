﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(MVC_application.Hubs.SignalRChat))]

namespace MVC_application.Hubs
{
    public class SignalRChat
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}

﻿using ApplicationServices;
using DomainCore;
using MVC_application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_application.Controllers
{
    public class ShoppingCartController : BaseController
    {
        IGoodBO goodBO;
        IEnumerable<Good> goods;

        public ShoppingCartController(IGoodBO goodBO)
        {
            this.goodBO = goodBO;

            goods = this.goodBO.GetGoods();

        }
        public ActionResult Index()
        {
            if (Session.Count == 0)
                //return Content("Cart is Empty");
                return RedirectToAction("Index", "Home");
            return View();
        }

        public ActionResult AddToCart(int? id)
        {
            if (id == null)
                return RedirectToAction("NotFound"); //return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            var good = goods.SingleOrDefault(c => c.Id == id);
            OrderItem orderItem = new OrderItem() { Good = good, Quantity = 1 };
            if (Session["cart"] == null)
            {
                Cart cart = new Cart(good);
                Session["cart"] = cart;
            }
            else
            {
                Cart cart = (Cart)Session["cart"];
                if (ifExist(id))
                {
                    cart.OrderItems.Find(c => c.Good.Id == id).Quantity++;
                }
                else
                {
                    cart.OrderItems.Add(orderItem);
                }
            }
            return RedirectToAction("Index");
        }
        
        public ActionResult DeleteFromCart(int? id)
        {
            if (id == null)
                return RedirectToAction("NotFound");
            Cart cart = (Cart)Session["cart"];
            if (ifExist(id) && cart.OrderItems.Find(c => c.Good.Id == id).Quantity>1 && cart.OrderItems.Exists(c => c.Good.Id == id))
            {
                cart.OrderItems.Find(c => c.Good.Id == id).Quantity--;
            }
            else
            {
                cart.OrderItems.Remove(cart.OrderItems.Find(c => c.Good.Id == id));
            }
            return RedirectToAction("Index");
        }

        private bool ifExist(int? id)
        {
            Cart cart = (Cart)Session["cart"];
            foreach (var orderitem in cart)
            {
                if (orderitem.Good.Id == id)
                    return true;
            }
            return false;


        }



        //public ActionResult AddToCart(int id)
        //{
        //    var good = goods.SingleOrDefault(c => c.Id == id);
        //    if (Session["cart"] == null)
        //    {
        //        List<Cart> cart = new List<Cart>()
        //        {
        //            new Cart(good,1)
        //        };
        //        Session["cart"] = cart;
        //    }
        //    else
        //    {
        //        List<Cart> cart = (List<Cart>)Session["cart"];
        //        cart.Add(new Cart(good, 1));
        //    }
        //    return View("Index");
        //}
    }
}
﻿using ApplicationServices;
using DAL;
using DomainCore;
using DomainServices.Repositories;
using MVC_application.Models;
using Repositories;
using Shared.HTTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_application.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        IHTTPManager manager;

        public HomeController(IHTTPManager manager)
        {
            this.manager = manager;
        }
        public ActionResult Index(int page = 1)
        {
            var result = manager.Get<IEnumerable<Good>>("api/goods");

            if (!result.IsSuccess)
            {
                return InternalServerError();
            }

            var model = facade.Execute(GetGoodsModel, page, result.Data.ToList(), 2);

            if (!facade.IsOk)
            {
                return InternalServerError();
            }

            return View(model);
        }

        private GoodsModel GetGoodsModel(int page, List<Good> goods, int pageSize = 2)
        {
            var model = new GoodsModel()
            {
                Goods = GetGoodsForPage(page, goods),
                PageInfo = GetInfo(page, goods)
            };

            return model;
        }
        private IEnumerable<Good> GetGoodsForPage(int page, List<Good> goods, int pageSize = 2)
        {
            return goods.Skip((page - 1) * pageSize).Take(pageSize);
        }

        private PageInfo GetInfo(int page, List<Good> goods, int pageSize = 2)
        {
            return new PageInfo()
            {
                CurrentNumber = page,
                TotalQuantity = goods.Count<Good>(),
                PageSize = pageSize,
                PagesCount = (int)Math.Ceiling((decimal)goods.Count<Good>() / pageSize)
            };
        }

        //public ActionResult GoodDetails(int id = default(int))
        //{
        //    if (id == 0)
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }
        //    else
        //    {
        //        var goods = facade.Execute(goodBO.GetGoods);

        //        if (!facade.IsOk)
        //        {
        //            return InternalServerError();
        //        }

        //        return View(goods.Single(c => c.Id == id));
        //    }

        //}
    }
}
﻿using ApplicationServices.Identity;
using DomainCore.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MVC_application.Models.Authentification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC_application.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var userManager = HttpContext.GetOwinContext().GetUserManager<UserBO>();
            var authManager = HttpContext.GetOwinContext().Get<SigninBO>();

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            SignInStatus result = await authManager.PasswordSignInAsync(model.UserName, model.Password, true, shouldLockout: false);

            
            switch (result)
            {
                case SignInStatus.Success:
                    return Redirect(returnUrl ?? Url.Action("Index", "Home"));
                case SignInStatus.LockedOut:
                    return View("Lockout");

                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

       // [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Login");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var userManager = HttpContext.GetOwinContext().GetUserManager<UserBO>();
            var authManager = HttpContext.GetOwinContext().Get<SigninBO>();

            var identityResult = await userManager.CreateAsync(
                new DomainCore.Identity.User()
                {
                    Email = model.Email,
                    UserName = model.UserName,
                    LockoutEnabled = false
                },
                model.Password
                );

            if (identityResult.Succeeded)
            {
                var user = userManager.FindByEmail(model.Email);
                authManager.SignIn(user, true, true);

                return RedirectToAction("Index", "Home");
            }

            ModelState.AddModelError("", "Something went wrong");
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> ChangePassword(ChangePassword model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var userManager = HttpContext.GetOwinContext().GetUserManager<UserBO>();
            var authManager = HttpContext.GetOwinContext().Get<SigninBO>();

            var user = userManager.FindByName(model.UserName);
            if (user== null)
            {
                return View();
            }
            userManager.ChangePassword(user.Id, model.OldPassword, model.NewPassword);
            authManager.SignIn(user, true, true);

            return RedirectToAction("Index", "Home  ");
           
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

    }
}
﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_application.Controllers
{
    public class BaseController: Controller
    {
        protected CommonFacade facade;
        public BaseController()
        {
            facade = new CommonFacade();
        }

        public virtual ActionResult InternalServerError()
        {
            return new HttpStatusCodeResult(500);

        }
    }
}
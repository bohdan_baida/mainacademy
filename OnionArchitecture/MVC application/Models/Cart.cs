﻿using DomainCore;
using System.Collections;
using System.Collections.Generic;

namespace MVC_application.Models
{
    public class Cart:IEnumerable<OrderItem>
    {
        public List<OrderItem> OrderItems { get; set; }

        public IEnumerator<OrderItem> GetEnumerator()
        {
            return OrderItems.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return OrderItems.GetEnumerator();
        }

        public Cart(Good good)
        {
            OrderItem orderItem = new OrderItem { Good = good, Quantity = 1 };
            
            OrderItems = new List<OrderItem>();
            OrderItems.Add(orderItem);
        }

        //public Good Good { get; set; }

        //public int Quantity { get; set; }

        //public Cart(Good good, int quantity)
        //{
        //    Good = good;
        //    Quantity = quantity;
        //}

    }
}
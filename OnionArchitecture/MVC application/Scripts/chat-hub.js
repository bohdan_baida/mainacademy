﻿$(document).ready(
    function () {
        //Declare a proxy to reference hub
        var chat = $.connection.chatHub;

        chat.client.broadcastMessage = function (name, message) {
            var encodedName = $('<div />').text(name).html();
            var encodedMessage = $('<div />').text(message).html();
            if (name == document.getElementById("displayname").value) {
                $('#discussion').append('<div class="chat self"><br><div class="user-name"><strong>' + encodedName + '</strong></div><br><p class="chat-message">' + encodedMessage + '</p></div>');
            } else
                $('#discussion').append('<div class="chat others"><br><div class="user-name"><strong>' + encodedName + '</strong></div><br><p class="chat-message">' + encodedMessage + '</p></div>');

                //$('#discussion').append('<li><strong>' + encodedName + '</strong>:&nbsp;&nbsp;' + encodedMessage + '</li>');
        }

        $('#message').focus();

        $.connection.hub.start().done(function () {
            $('#sendmessage').click(function () {
                chat.server.send($('#displayname').val(), $('#message').val());
                $('#message').val('').focus();
            });
            $('#show-chat').click(function () {
                var chatBodyNode = $('.js-chat-body');
                if (chatBodyNode.hasClass('hidden')) {
                    chatBodyNode.removeClass('hidden');
                    document.getElementById("show-chat").value = 'Hide Chat';
                } else {
                    chatBodyNode.removeClass('value');
                    chatBodyNode.addClass('hidden');
                    document.getElementById("show-chat").value = 'Show Chat';
                }
            });
        });
    });
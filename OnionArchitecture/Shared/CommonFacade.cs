﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class CommonFacade
    {
        public List<string> Errors = new List<string>();

        public bool IsOk => Errors.Count == 0;

        public TResult Execute<TParameters, TResult>(Func<TParameters, TResult> method, TParameters parameters)
            where TResult : new()
        {
            var result = new TResult();
            try
            {
                result = method(parameters);

                LogMethodExecution(method.Method.Name);
            }
            catch (Exception ex)
            {
                LogError(method.Method.Name, ex.Message);
            }
            return result;
        }

        public TResult Execute<TResult>(Func<TResult> method)
            where TResult : new()
        {
            var result = new TResult();
            try
            {
                result = method();

                LogMethodExecution(method.Method.Name);
            }
            catch (Exception ex)
            {
                LogError(method.Method.Name, ex.Message);
            }
            return result;
        }

        public TResult Execute<TResult, P1, P2, P3>(Func<P1, P2, P3, TResult> method, P1 param1, P2 param2, P3 param3)
           where TResult : new()
        {
            var result = new TResult();
            try
            {
                result = method.Invoke(param1, param2, param3);

                LogMethodExecution(method.Method.Name);
            }
            catch (Exception ex)
            {
                LogError(method.Method.Name, ex.Message);
            }
            return result;
        }

        public void Execute<TParameters>(Action<TParameters> method, TParameters parameters)
        {
            try
            {
                method(parameters);

                LogMethodExecution(method.Method.Name);
            }
            catch (Exception ex)
            {
                LogError(method.Method.Name, ex.Message);
            }
        }

        public void Execute(Action method)
        {
            try
            {
                method();

                LogMethodExecution(method.Method.Name);
            }
            catch (Exception ex)
            {
                LogError(method.Method.Name, ex.Message);
            }
        }

        private void LogMethodExecution(string methodName)
        {
            Logger.LogMethodExecution(methodName);
        }
        private void LogError(string methodName, string exception)
        {
            Logger.LogError(methodName, exception);

            Errors.Add(exception);
        }
    }
}

﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public static class Logger
    {
        static Serilog.Core.Logger logger;
        private static string messageTemplate = "method:{method} message:{message}";

        static Logger()
        {
            logger = new LoggerConfiguration()
            .WriteTo.File("log.txt")
            .CreateLogger();
        }

        public static void LogError(string method, string message)
        {
            logger.Error(messageTemplate, method, message);

        }

        public static void LogMethodExecution(string method)
        {
            logger.Information(messageTemplate, method);
        }

        

            
    }
}

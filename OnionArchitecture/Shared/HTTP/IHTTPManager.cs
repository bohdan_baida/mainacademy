﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shared.HTTP
{
    public interface IHTTPManager
    {
        IResponse<TResult> Get<TResult>(string url, object data = null, Dictionary<string, string> headers = null);
        IResponse Post(string url, object data= null, Dictionary<string, string> headers = null);

        Task<IResponse<TResult>> GetAsync<TResult>(string url, object data = null, Dictionary<string, string> headers = null);
        Task<IResponse> PostAsync(string url, object data = null, Dictionary<string, string> headers = null);
    }
}

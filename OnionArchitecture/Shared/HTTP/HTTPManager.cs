﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Shared.HTTP
{
    public class HTTPManager : IHTTPManager
    {
        private readonly string scheme;
        private readonly string host;

        public HTTPManager(string scheme, string host)
        {
            this.scheme = scheme;
            this.host = host;
        }
        public IResponse<TResult> Get<TResult>(string url, object data = null, Dictionary<string, string> headers = null)
            => this.SendRequest<TResult>(url, HttpMethodEnum.GET, data, headers);
        public IResponse Post(string url, object data = null, Dictionary<string, string> headers = null)
            => this.SendRequest(url, HttpMethodEnum.POST, data, headers);

        public Task<IResponse<TResult>> GetAsync<TResult>(string url, object data = null, Dictionary<string, string> headers = null)
            => Task.Run(() => this.SendRequest<TResult>(url, HttpMethodEnum.GET, data, headers));
        public Task<IResponse> PostAsync(string url, object data = null, Dictionary<string, string> headers = null)
            => Task.Run(() => this.SendRequest(url, HttpMethodEnum.POST, data, headers));

        private IResponse<TResult> Send<TResult>(string url, HttpMethodEnum httpMethod, object data = null, Dictionary<string, string> headers = null)
            => this.SendRequest<TResult>(url, httpMethod, data, headers);
        private IResponse Send(string url, HttpMethodEnum httpMethod, object data = null, Dictionary<string, string> headers = null)
            => this.SendRequest(url, httpMethod, data, headers);

        private Task<IResponse<TResult>> SendAsync<TResult>(string url, HttpMethodEnum httpMethod, object data = null, Dictionary<string, string> headers = null)
            => Task.Run(() => this.SendRequest<TResult>(url, httpMethod, data, headers));
        private Task<IResponse> SendAsync(string url, HttpMethodEnum httpMethod, object data = null, Dictionary<string, string> headers = null)
            => Task.Run(() => this.SendRequest(url, httpMethod, data, headers));

        private IResponse<TResult> SendRequest<TResult>(string url, HttpMethodEnum httpMethod, object data, Dictionary<string, string> headers = null)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri($"{this.scheme}://{this.host}");

                if (headers != default(Dictionary<string, string>))
                {
                    foreach (var header in headers)
                        client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }

                return this.Execute<TResult>(client, url, httpMethod, data);
            }
        }
        private IResponse SendRequest(string url, HttpMethodEnum httpMethod, object data, Dictionary<string, string> headers = null)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri($"{this.scheme}://{this.host}");
                if (headers != default(Dictionary<string, string>))
                {
                    foreach (var header in headers)
                        client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }

                return this.Execute(client, url, httpMethod, data);
            }
        }

        private IResponse<TResult> Execute<TResult>(HttpClient client, string url, HttpMethodEnum httpMethod, object data)
        {
            switch (httpMethod)
            {
                case HttpMethodEnum.GET:
                    var queryParameters = this.GetQueryParameters(data);
                    if (queryParameters != null)
                        url = $"{url}?{queryParameters}";

                    return new Response<TResult>(client.GetAsync(url).Result);
                case HttpMethodEnum.POST:
                    var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");

                    return new Response<TResult>(client.PostAsync(url, content).Result);

                default: throw new NotImplementedException($"Http method *{httpMethod.ToString()}* doesn't supported");
            }
        }
        private IResponse Execute(HttpClient client, string url, HttpMethodEnum httpMethod, object data)
        {
            switch (httpMethod)
            {
                case HttpMethodEnum.GET:
                    var queryParameters = this.GetQueryParameters(data);
                    if (queryParameters != null)
                        url = $"{url}?{queryParameters}";

                    return new Response(client.GetAsync(url).Result);
                case HttpMethodEnum.POST:
                    var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");

                    return new Response(client.PostAsync(url, content).Result);

                default: throw new NotImplementedException($"Http method *{httpMethod.ToString()}* doesn't supported");
            }
        }

        private string GetQueryParameters(object data)
        {
            if (data == null)
                return null;

            var properties = data.GetType().GetProperties();

            var result = String.Empty;
            foreach (var property in properties)
                result += $"{property.Name}={property.GetValue(data)}&";

            if (string.IsNullOrEmpty(result))
                return null;

            return result.Remove(result.LastIndexOf("&"));
        }
    }
}

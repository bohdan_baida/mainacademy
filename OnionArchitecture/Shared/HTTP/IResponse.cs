﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Shared.HTTP
{
    public interface IResponse<TResult> : IResponse
    {
        TResult Data { get; }
    }

    public interface IResponse
    {
        Status Status { get; }
        HttpStatusCode StatusCode { get; }
        bool IsSuccess { get; }
    }
}

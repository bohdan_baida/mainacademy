﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Shared.HTTP
{
    public class Response<TResult>: Response, IResponse<TResult>
    {
        public TResult Data { get; }
        public new bool IsSuccess => this.Status == Status.Success && this.Data != null;

        public Response(Status status, TResult data)
            : base(status) => this.Data = data;

        public Response(HttpResponseMessage responseMessage) : base(responseMessage)
        {
            var contentType = responseMessage.Content.Headers.ContentType.MediaType;
            if (!string.IsNullOrEmpty(contentType))
            {
                switch (contentType)
                {
                    case "application/json":
                        this.Data = responseMessage.StatusCode != HttpStatusCode.NoContent
                            ? JsonConvert.DeserializeObject<TResult>(responseMessage.Content.ReadAsStringAsync().Result)
                            : default(TResult);
                        break;
                    default: throw new NotImplementedException($"Content type *{contentType}* doesn't supported");
                }
            }
        }
    }

    public class Response : IResponse
    {
        public Status Status { get; }
        public HttpStatusCode StatusCode { get; } = HttpStatusCode.OK;
        public bool IsSuccess => this.Status == Status.Success;

        public Response(Status status) => this.Status = status;

        public Response(HttpResponseMessage responseMessage)
        {
            this.StatusCode = responseMessage.StatusCode;
            this.Status = responseMessage.IsSuccessStatusCode ? Status.Success : Status.Error;
        }
    }
}

﻿using DAL;
using DomainCore.Interfaces;
using DomainServices.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainCore;

namespace Repositories
{
    public class Repository<T> : IRepository<T>
        where T : class, IEntity
    {
        private IShopContext context;
        private DbSet<T> dbSet;

        public Repository(IShopContext context)
        {
            this.context = context;
            dbSet = ((DbContext)this.context).Set<T>();
        }

     

        public void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public T Get(int id)
        {
            return dbSet.SingleOrDefault(c => c.Id == id);
        }

        public List<T> GetAll()
        {
            return dbSet.ToList();
        }

        public void Remove(int id)
        {
            var entity = Get(id);

            dbSet.Remove(entity);
        }
    }
}

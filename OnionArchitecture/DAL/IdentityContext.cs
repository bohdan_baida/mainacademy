﻿using DomainCore.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class IdentityContext : IdentityDbContext<User>
    {
        public IdentityContext(string connectionString)
            : base(connectionString)
        {

        }
    }
}

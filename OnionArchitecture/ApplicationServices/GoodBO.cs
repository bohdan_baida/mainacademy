﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainCore;
using DomainServices.Repositories;
using Repositories;

namespace ApplicationServices
{
    public class GoodBO : IGoodBO
    {
        IGoodRepository repository;
        public GoodBO(IGoodRepository repository)
        {
            this.repository = repository;
        }
        public List<Good> GetGoods()
        {
            var goods = repository.GetAll();
            return goods;
        }
    }
}

﻿using DAL;
using DomainCore.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.Identity
{
    public class UserBO : UserManager<User>
    {
        public UserBO(IUserStore<User> store)
            : base(store)
        {
        }

        // this method is called by Owin therefore this is the best place to configure your User Manager
        public static UserBO Create(
            IdentityFactoryOptions<UserBO> options, IOwinContext context)
        {
            var manager = new UserBO(
                new UserStore<User>(context.Get<IdentityContext>()));


            return manager;
        }
    }

    
}

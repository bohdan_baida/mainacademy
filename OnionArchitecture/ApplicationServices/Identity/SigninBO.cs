﻿using DomainCore.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.Identity
{
    public class SigninBO : SignInManager<User, string>
    {
        public SigninBO(UserBO userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(User user)
        {
            return user.GenerateUserIdentityAsync((UserBO)UserManager);
        }

        public static SigninBO Create(IdentityFactoryOptions<SigninBO> options, IOwinContext context)
        {
            return new SigninBO(context.GetUserManager<UserBO>(), context.Authentication);
        }
    }
}

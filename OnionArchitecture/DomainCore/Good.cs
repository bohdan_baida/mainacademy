﻿using DomainCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainCore
{
    [Table("dbo.Goods")]
    public class Good : IEntity
    {
        public Good()
        {
            OrderItems = new HashSet<OrderItem>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        //[NotMapped]
        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }
}

﻿using ApplicationServices;
using DomainCore;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ShopApi.Controllers
{
    public class GoodsController : ApiController
    {
        IGoodBO goodBO;
        CommonFacade facade;

        public GoodsController(IGoodBO goodBO)
        {
            this.goodBO = goodBO;
            facade = new CommonFacade();

        }
        public IEnumerable<Good> GetGoods()
        {
            return goodBO.GetGoods();
        }

        public Good PostGood([FromBody]Good good
            //, [FromUri]string token
            )
        {
            //tipa chekaem token
            ///some good adding to db
            return good;
        }
    }
}
